﻿using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Jumper.ContentObject {
	public static class Fonts {
		public static void LoadFonts ( ContentManager content ) {
			DebugFont = content.Load<SpriteFont> ( "Font/DebugFont" );
			MusicFont = content.Load<SpriteFont> ( "Font/MusicFont" );
			TextFont = content.Load<SpriteFont> ( "Font/Font" );
		}

		public static SpriteFont DebugFont { get; set; }
		public static SpriteFont MusicFont { get; set; }
		public static SpriteFont TextFont { get; set; }

		public static void PrintDebugInfo ( SpriteBatch spriteBatch, IDebug debugObject ) {

			string str = debugObject.GetDebugText ( );
			Vector2 midPoint = DebugFont.MeasureString ( str ) / 2;

			spriteBatch.DrawString ( DebugFont, str,
				new Vector2 ( midPoint.X, 0 + midPoint.Y ),
				Color.Black, 0, midPoint, 1, SpriteEffects.None, 1 );
		}
	}
}
