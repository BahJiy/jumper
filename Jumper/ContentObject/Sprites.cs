using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Jumper.ContentObject {
	public static class Sprites {
		public static void LoadSprite ( ContentManager content ) {
			LoadCustomSprite ( content );

			if ( PlayerSprite == null ) {
				PlayerSprite = content.Load<Texture2D> ( "Image/Circle" );
			}
			if ( HorizontalWallSprite == null ) {
				HorizontalWallSprite = content.Load<Texture2D> ( "Image/Wall" );
			}
			if ( VerticalWallSprite == null ) {
				VerticalWallSprite = content.Load<Texture2D> ( "Image/sWall" );
			}
		}

		private static void LoadCustomSprite ( ContentManager content ) {
			// TODO Implement load of custom sprite
		}

		public static Texture2D PlayerSprite { get; set; }
		public static Texture2D HorizontalWallSprite { get; set; }
		public static Texture2D VerticalWallSprite { get; set; }
	}
}
