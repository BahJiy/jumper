﻿namespace Jumper {
	public static class Program {
		static void Main(string[] args) => new JumperGame().Run();
	}
}
