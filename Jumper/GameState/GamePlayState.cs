﻿using System;
using Jumper.ContentObject;
using Jumper.GameObject;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Jumper.GameState {
	public class GamePlayState : IGameState {
		private PlayerObject player { get; set; }
		private bool initialized { get; set; } = false;

		public GamePlayState ( ) {
			player = new PlayerObject ( Sprites.PlayerSprite );
		}
		public void Update ( GameTime gameTime ) {
			player.Update ( gameTime );
		}
		public void Draw ( SpriteBatch spriteBatch, GameTime gameTime ) {
			player.Draw ( spriteBatch, gameTime );
		}
		public void Initialize ( ) {
			player.Initialize ( );

			initialized = true;
		}

		public bool IsInitialized ( ) => initialized;

		public string GetDebugText ( ) {
			return player.GetDebugText ( );
		}
	}
}
