﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Jumper.GameState {
	public interface IGameState : IDebug {
		public virtual void Initialize ( ) { }
		public virtual void Uninitialized ( ) { }
		public abstract void Update ( GameTime gameTime );
		public abstract void Draw ( SpriteBatch spriteBatch, GameTime gameTime );
		public abstract bool IsInitialized ( );
	}
}
