﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Jumper {
	public static class Configuration {
		public static Vector2 ScreenSize { get; private set; } = new Vector2 ( 1500, 750 );
		public static bool DEBUG { get; set; } = true;
		public static Keys JumpButton { get; set; } = Keys.Space;

		#region Movement Information
		public static Vector2 ElasticityFactor { get; set; } = new Vector2 ( -0.3f );
		public static float MaxJumpSpeed { get; set; } = 12;
		public static float Gravity { get; set; } = 9.14f;
		public static float TerminalVelocity { get; set; } = 50;
		#endregion
	}
}
