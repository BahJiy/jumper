namespace Jumper {
	public interface IDebug {
		public string GetDebugText ( );
	}
}
