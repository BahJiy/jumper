﻿
using System.Text;
using Jumper.ContentObject;
using Jumper.GameState;
using Jumper.Input;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Jumper {
	public class JumperGame : Game, IDebug {
		public static Viewport SCREEN { get; private set; }
		private readonly GraphicsDeviceManager _graphicsDeviceManager;
		private SpriteBatch _spriteBatch;

		public GamePlayState gamePlayState { get; private set; }
		private IGameState gameState { get; set; }

		public JumperGame ( ) {
			_graphicsDeviceManager = new GraphicsDeviceManager ( this );
			Content.RootDirectory = "Content";
			IsMouseVisible = true;
		}

		/// <summary>
		/// Initialized all game variables
		/// </summary>
		protected override void Initialize ( ) {
			_graphicsDeviceManager.PreferredBackBufferWidth = ( int ) Configuration.ScreenSize.X;
			_graphicsDeviceManager.PreferredBackBufferHeight = ( int ) Configuration.ScreenSize.Y;
			_graphicsDeviceManager.ApplyChanges ( );

			SCREEN = _graphicsDeviceManager.GraphicsDevice.Viewport;

			base.Initialize ( );
		}

		/// <summary>
		/// Initialized the external game files
		/// </summary>
		protected override void LoadContent ( ) {
			_spriteBatch = new SpriteBatch ( GraphicsDevice );

			Fonts.LoadFonts ( Content );
			Sprites.LoadSprite ( Content );

			#region Initialize Game States
			gamePlayState = new GamePlayState ( );
			#endregion

			// TODO
			SetGameState ( gamePlayState );

			base.LoadContent ( );
		}

		/// <summary>
		/// Unload and release external game files data
		/// </summary>
		protected override void UnloadContent ( ) {
		}

		protected override void Update ( GameTime gameTime ) {
			KeyboardInput.Update ( );
			MouseButtonInput.Update ( );

			if ( gameState.IsInitialized ( ) ) {
				gameState.Update ( gameTime );
			}

			base.Update ( gameTime );
		}
		protected override void Draw ( GameTime gameTime ) {
			GraphicsDevice.Clear ( Color.GhostWhite );
			_spriteBatch.Begin ( );

			if ( Configuration.DEBUG ) {
				Fonts.PrintDebugInfo ( _spriteBatch, this );
			}

			if ( gameState.IsInitialized ( ) ) {
				gameState.Draw ( _spriteBatch, gameTime );
			}

			_spriteBatch.End ( );

			base.Draw ( gameTime );
		}

		public void SetGameState ( IGameState newGameState ) {
			gameState?.Uninitialized ( );

			newGameState.Initialize ( );
			gameState = newGameState;
		}

		public string GetDebugText ( ) {
			StringBuilder str = new StringBuilder ( );

			MouseState mouse = Mouse.GetState ( );

			str.Append ( $"Mouse Pos: X {mouse.X} | {mouse.Y}\n" )
				.Append ( $"Left Button: {mouse.LeftButton} | Right Button: {mouse.RightButton}\n" );

			if ( gameState != null && gameState.IsInitialized ( ) ) {
				str.Append ( gameState.GetDebugText ( ) );
			}
			return str.ToString ( );
		}
	}
}
