﻿using System;

using Microsoft.Xna.Framework.Input;

namespace Jumper.Input {
	/// <summary>
	/// Wrapper around the KeyboardState and handles check key states
	/// </summary>
	public static class KeyboardInput {
		/// <summary>
		/// Event handler for if key(s) is/are pressed
		/// </summary>
		public static event EventHandler<KeyboardEventArgs> KeyStateChangedEvent;
		public static event EventHandler<KeyboardEventArgs> KeyDownEvent;

		private static KeyboardState _CURRENTSTATE;
		private static KeyboardState _PREVIOUSSTATE;

		/// <summary>
		/// Update the internal keyboard state
		/// </summary>
		public static void Update ( ) {
			_PREVIOUSSTATE = _CURRENTSTATE;
			_CURRENTSTATE = Keyboard.GetState ( );

			// If keys are pressed, invoke the handlers
			foreach ( Keys key in Enum.GetValues ( typeof ( Keys ) ) ) {
				if ( StateChanged ( key ) ) {
					KeyStateChangedEvent?.Invoke ( null, new KeyboardEventArgs ( _CURRENTSTATE, key, IsDown ( key ) ) );
				}
				if ( IsDown ( key ) ) {
					KeyDownEvent?.Invoke ( null, new KeyboardEventArgs ( _CURRENTSTATE, key, true ) );
				}
			}
		}

		/// <summary>
		/// Get if the key state changed
		/// </summary>
		/// <param name="button">Key to check</param>
		/// <returns>True if key changed (Pressed to Release; vice versa)</returns>
		public static bool StateChanged ( Keys key )
			=> ( _CURRENTSTATE.IsKeyDown ( key ) && _PREVIOUSSTATE.IsKeyUp ( key ) )
			|| ( _CURRENTSTATE.IsKeyUp ( key ) && _PREVIOUSSTATE.IsKeyDown ( key ) );

		/// <summary>
		/// Get if key is just pressed and NOT held down
		/// </summary>
		/// <param name="key">Key to check</param>
		/// <returns>True if key is just pressed and NOT held</returns>
		public static bool IsPressed ( Keys key ) => _CURRENTSTATE.IsKeyDown ( key ) && _PREVIOUSSTATE.IsKeyUp ( key );

		/// <summary>
		/// Get if key is pressed or is held down
		/// </summary>
		/// <param name="key">Key to check</param>
		/// <returns>True if key is pressed or held</returns>
		public static bool IsDown ( Keys key ) => _CURRENTSTATE.IsKeyDown ( key );

		/// <summary>
		/// Get if key is NOT pressed
		/// </summary>
		/// <param name="key"></param>
		/// <returns>True if key is NOT pressed</returns>
		public static bool IsUp ( Keys key ) => _CURRENTSTATE.IsKeyUp ( key );

	}

	public class KeyboardEventArgs : EventArgs {
		public readonly KeyboardState KeyboardState;
		public readonly Keys Key;
		public readonly bool IsDown;

		public KeyboardEventArgs ( KeyboardState keyboardState, Keys key, bool isDown ) {
			KeyboardState = keyboardState;
			Key = key;
			IsDown = isDown;
		}
	}
}
