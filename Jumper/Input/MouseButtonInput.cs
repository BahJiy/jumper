﻿using System;

using Microsoft.Xna.Framework.Input;

namespace Jumper.Input {
	/// <summary>
	/// Wrapper around the MouseState and handles check mouse button states
	/// </summary>
	public static class MouseButtonInput {

		public static event EventHandler<MouseButtonEventArgs> MouseButtonStateChangedEvent;
		public static event EventHandler<MouseButtonEventArgs> MouseButtonDownEvent;

		private static MouseState _CURRENTSTATE;
		private static MouseState _PREVIOUSSTATE;

		/// <summary>
		/// Update the internal mouse state
		/// </summary>
		public static void Update ( ) {
			_PREVIOUSSTATE = _CURRENTSTATE;
			_CURRENTSTATE = Mouse.GetState ( );

			foreach ( MouseButton button in Enum.GetValues ( typeof ( MouseButton ) ) ) {
				if ( StateChanged ( button ) ) {
					MouseButtonStateChangedEvent?.Invoke ( null, new MouseButtonEventArgs (
						_CURRENTSTATE, button, _CURRENTSTATE.Get ( button ) ) );
				}

				if ( IsDown ( button ) ) {
					MouseButtonDownEvent?.Invoke ( null, new MouseButtonEventArgs (
						_CURRENTSTATE, button, ButtonState.Pressed ) );
				}
			}
		}

		/// <summary>
		/// Get if the button state changed
		/// </summary>
		/// <param name="button">Mouse button to check</param>
		/// <returns>True if mouse button changed (Pressed to Release; vice versa)</returns>
		public static bool StateChanged ( MouseButton button )
			=> _CURRENTSTATE.Get ( button ) != _PREVIOUSSTATE.Get ( button );

		/// <summary>
		/// Get if button is just pressed and NOT held down
		/// </summary>
		/// <param name="button">Mouse button to check</param>
		/// <returns>True if mouse ubbton is pressed and NOT held down</returns>
		public static bool IsPressed ( MouseButton button )
			=> _CURRENTSTATE.Get ( button ) == ButtonState.Pressed && _PREVIOUSSTATE.Get ( button ) == ButtonState.Released;

		/// <summary>
		/// Get if button is pressed or is held down
		/// </summary>
		/// <param name="button">Mouse button to check</param>
		/// <returns>True if mouse button is pressed or held down</returns>
		public static bool IsDown ( MouseButton button ) => _CURRENTSTATE.Get ( button ) == ButtonState.Pressed;

		/// <summary>
		/// Get if button is NOT pressed
		/// </summary>
		/// <param name="button">Mouse button to check</param>
		/// <returns>True if mouse button is no pressed</returns>
		public static bool IsUp ( MouseButton button ) => _CURRENTSTATE.Get ( button ) == ButtonState.Released;

		public static ButtonState Get ( this MouseState mouse, MouseButton button ) => button switch {
			MouseButton.RightButton => mouse.RightButton,
			MouseButton.LeftButton => mouse.LeftButton,
			MouseButton.MiddleButton => mouse.MiddleButton,
			_ => throw new NotImplementedException ( )
		};
	}

	public enum MouseButton {
		RightButton,
		LeftButton,
		MiddleButton
	}

	public class MouseButtonEventArgs : EventArgs {
		public readonly MouseState MouseState;
		public readonly MouseButton Button;
		public readonly ButtonState State;

		public MouseButtonEventArgs ( MouseState mouseState, MouseButton button, ButtonState state ) {
			MouseState = mouseState;
			Button = button;
			State = state;
		}
	}
}
