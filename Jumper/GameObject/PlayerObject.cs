﻿using System.Collections.Generic;
using System.Text;

using Jumper.Input;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Jumper.GameObject {
	public sealed class PlayerObject : GameObject {
		/// <summary>
		/// The elapsed game time from last updated. Used to provide a smoother
		/// movement based on the time
		/// </summary>
		private float elapsedGameTime { get; set; } = 0;

		/// <summary>
		/// The player's movement velocity. The X movement should always be zero
		/// </summary>
		private Vector2 _velocity = Vector2.Zero;

		private Vector2[ ] PositionHistory;

		// TODO: Remove once Music class exists
		private readonly int _JUMP_AMOUNT = 60;
		private readonly int _SPACE_AMOUNT = 13;
		private readonly float _JUMP_SCALE_AMOUNT = 5.50f;
		private readonly float _GRAVITY_SCALE_AMOUNT = 1.31f;

		public PlayerObject ( Texture2D sprite ) : base ( sprite ) {
			Position = new Vector2 ( JumperGame.SCREEN.Width / 7, JumperGame.SCREEN.Height / 2 );
			Scale = Vector2.One;

			PositionHistory = new Vector2[ 5 ];
		}

		/// <summary>
		/// Set up the input hooks
		/// </summary>
		public override void Initialize ( ) {
			KeyboardInput.KeyStateChangedEvent += CheckKeyPress;
		}

		/// <summary>
		/// Remove input hooks
		/// </summary>
		public override void Uninitialize ( ) {
			KeyboardInput.KeyStateChangedEvent -= CheckKeyPress;
		}

		public override void Draw ( SpriteBatch spriteBatch, GameTime gameTime ) {
			for ( int i = PositionHistory.Length - 1; i >= 0; --i ) {
				Vector2 tail = new Vector2 ( PositionHistory[ i ].X - i * _SPACE_AMOUNT, PositionHistory[ i ].Y );
				spriteBatch.Draw ( SPRITE, tail, null, Color.White, 0.0f, Vector2.Zero, Scale, SpriteEffects.None, 0 );
			}
			spriteBatch.Draw ( SPRITE, Position, null, Color.White, 0.0f, Vector2.Zero, Scale, SpriteEffects.None, 0 );
		}

		public override void Update ( GameTime gameTime ) {
			elapsedGameTime = ( float ) gameTime.ElapsedGameTime.TotalSeconds;

			_velocity.Y += Configuration.Gravity * elapsedGameTime * _GRAVITY_SCALE_AMOUNT;
			if ( _velocity.Y > Configuration.TerminalVelocity ) {
				_velocity.Y = Configuration.TerminalVelocity;
			}

			// Update the position history before moving the player
			for ( int i = PositionHistory.Length - 1; i >= 0; --i ) {
				if ( i != 0 ) {
					PositionHistory[ i ] = PositionHistory[ i - 1 ];
				} else {
					PositionHistory[ 0 ] = Position;
				}
			}

			Position += _velocity;

			if ( Position.Y < 0 ) {
				Position = new Vector2 ( Position.X, 0 );
				_velocity *= Configuration.ElasticityFactor;
			} else if ( Position.Y + SPRITE.Height > JumperGame.SCREEN.Height ) {
				Position = new Vector2 ( Position.X, JumperGame.SCREEN.Height - SPRITE.Height );
				_velocity *= Configuration.ElasticityFactor;
			}

		}

		private void CheckKeyPress ( object sender, KeyboardEventArgs e ) {
			if ( e.Key == Configuration.JumpButton && e.IsDown ) {
				Jump ( );
			}
		}

		private void Jump ( ) {
			_velocity -= new Vector2 ( 0, ( int ) ( _JUMP_AMOUNT * elapsedGameTime * _JUMP_SCALE_AMOUNT ) );

			if ( _velocity.Y > Configuration.MaxJumpSpeed ) {
				_velocity.Y = Configuration.MaxJumpSpeed;
			}
		}

		public override string GetDebugText ( ) {
			StringBuilder str = new StringBuilder ( );
			str.Append ( "PlayerObject:\n" )
				.Append ( $"Position: {Position} | Velocity {_velocity}" );
			return str.ToString ( );
		}
	}
}
