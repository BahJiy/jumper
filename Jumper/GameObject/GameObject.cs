﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Jumper.GameObject {
	/// <summary>
	/// A basic representation of a game object. Contains a texture, position and scaling
	/// </summary>
	public abstract class GameObject : IDebug {
		/// <summary>
		/// The texture of the object
		/// </summary>
		protected readonly Texture2D SPRITE;

		public Vector2 Position { get; set; }
		public Vector2 Scale { get; set; }

		/// <summary>
		/// Initialize a basic GameObject with a texture.
		/// Defaults position to Vector2.Zero and scale to Vector2.One
		/// </summary>
		/// <param name="sprite">The texture of the prite</param>
		protected GameObject ( Texture2D sprite ) : this ( sprite, Vector2.Zero, Vector2.One ) { }
		/// <summary>
		/// Initialize a GameObject with a texture and a position.
		/// Defaults scale to Vector2.One
		/// </summary>
		/// <param name="sprite">THe texture of the objectsprite</param>
		/// <param name="position">The intial position of the object</param>
		protected GameObject ( Texture2D sprite, Vector2 position ) : this ( sprite, position, Vector2.One ) { }
		/// <summary>
		/// Initialize a GameObject with a texture, position and scale
		/// </summary>
		/// <param name="sprite">The texture of the objectsprite</param>
		/// <param name="position">The intial position of the object</param>
		/// <param name="scale">The initial scaling of the object's texture</param>
		protected GameObject ( Texture2D sprite, Vector2 position, Vector2 scale ) {
			SPRITE = sprite;
			Position = position;
			Scale = scale;
		}

		/// <summary>
		/// Initialize the object's internal state. This function is optional, but will always be called
		/// </summary>
		public virtual void Initialize ( ) { }

		/// <summary>
		/// Uninitialize the object. This function is optional, but will always be called
		/// </summary>
		public virtual void Uninitialize ( ) { }

		/// <summary>
		/// Update the GameObject's internal state
		/// </summary>
		/// <param name="gameTime">The game time since last update call</param>
		public abstract void Update ( GameTime gameTime );

		/// <summary>
		/// Draw the GameObject on screen
		/// </summary>
		/// <param name="spriteBatch">The SpriteBatch to use. Draw must already have been started</param>
		/// <param name="gameTime">The game time since last draw call</param>
		public abstract void Draw ( SpriteBatch spriteBatch, GameTime gameTime );

		public abstract string GetDebugText ( );
	}
}
