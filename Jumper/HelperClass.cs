﻿using System;

namespace Jumper {
	public static class HelperClass {
		public static bool IsBetween<T> ( this T value, T from, T to, bool inclusive = true ) where T : IComparable<T> {
			if ( inclusive ) {
				return value.CompareTo ( from ) >= 0 && value.CompareTo ( to ) <= 0;
			} else {
				return value.CompareTo ( from ) > 0 && value.CompareTo ( to ) < 0;
			}
		}
	}
}
